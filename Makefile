all: tldr arch ietf

arch: arch-update arch-deploy
arch-init:
	git clone git://github.com/lahwaacz/arch-wiki-docs.git ./arch/
arch-update:
	cd arch && ./arch-wiki-docs.py --output-directory stuff --clean
arch-deploy:
	@#date > ./arch/stuff/generated.txt
	DOMAIN="archwiki.karmanyaah.malhotra.cc" DIR="./arch/stuff" ./deploy.sh


tldr: tldr-update tldr-deploy
tldr-update:
	git clone git://github.com/tldr-pages/tldr.git || (cd tldr && git pull)
tldr-deploy:
	@#date > tldr/generated.txt
	DOMAIN="tldr.karmanyaah.malhotra.cc" DIR="./tldr" ./deploy.sh


ietf: ietf-update ietf-deploy
ietf-update:
	rsync ftp.rfc-editor.org::rfcs/ --include '**.html' --include '**.txt' --include '**.txt.new' --include '**/'  --exclude '*' ./ietf -r --progress --delete --exclude=ien/scanned -u -l
	@# .txt.new for rfc-ref.txt.new symlink weird case
	echo -e "Link formats \n1. /rfc1234.txt \n2. /rfc1234.html \n3. /rfc-index.txt \nOR \n4. /bcp-index.txt \n5. /bcp/bcp123.txt \nwhere bcp can be replaced with "ien" or "std"\n\nSee /generated.txt for update time." | tee ietf/index.gmi ietf/index.txt > /dev/null
	echo "<pre>$$(cat ietf/index.txt)</pre>" > ietf/index.html
	sed 's|CREATED ON:.*$$|CREATED ON: --date removed from IPFS version for deduplication, /generated.txt might have some info depending on how you got these files--|' ./ietf/*index.txt ./ietf/*ref.txt -i
#for i in pdf txt json xml html; \
#	do mkdir -p ietf-out/rfc/$$i/ \
#	&& A=$$(mktemp) && ls -1 > $$A \
#	&& for k in $$(ls -1 ietf/ | grep -P "rfc[0-9]{1,5}.$$i" | grep -vF $$A); \
#		do ln -sf $$k ietf-out/rfc/$$i/$$(echo $$k | grep -o '[0-9]' | tr -d '\n'); \
#		echo $$k; \
#done; \
#rm $$A \
#; done

ietf-deploy:
	@#date > ietf/generated.txt
	DOMAIN="ietf.karmanyaah.malhotra.cc" DIR="./ietf" CUSTOM_IPFS_ADD_OPTIONS="--chunker=buzhash --dereference-args" ./deploy.sh

filecoin:
	ESTU_KEY=$$(cat estu.key) DOMAINS="ietf.karmanyaah.malhotra.cc archwiki.karmanyaah.malhotra.cc tldr.karmanyaah.malhotra.cc unifiedpush.org" ./pin.sh 
