#!/bin/bash
for i in $DOMAINS
do 
	CID=$(ipfs name resolve /ipns/$i)
       	CID=${CID#"/ipfs/"}
	curl -X POST https://api.estuary.tech/content/add-ipfs -d '{ "name": "'$i/$(date +%F_%T)'", "root": "'$CID'" }' -H "Content-Type: application/json" -H "Authorization: Bearer $ESTU_KEY"
	#echo curl -X POST https://api.estuary.tech/content/add-ipfs -d '{ "name": "'$i/$(date +%F_%T)'", "root": "'$CID'" }' -H "Content-Type: application/json" -H "Authorization: Bearer $ESTU_KEY"
done
